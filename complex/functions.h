#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QString>

QString sum(double a, double b, double c, double d);
QString minus(double a, double b, double c, double d);
QString mult (double a, double b, double c, double d);
QString div (double a, double b, double c, double d);
QString deg (double a, double b, int n);
QString sqrt (double a, double b, int n);

#endif // FUNCTIONS_H
