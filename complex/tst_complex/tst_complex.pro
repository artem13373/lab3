QT += testlib
QT -= gui
QT += widgets

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_complex.cpp \
    main.cpp

SOURCES += ../*.cpp

HEADERS += \
    tst_complex.h

HEADERS += ../*.h
