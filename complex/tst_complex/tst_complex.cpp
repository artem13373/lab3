#include <QtTest>
#include <../functions.h>
#include "tst_complex.h"


// add necessary includes here

Test_Complex::Test_Complex()
{

}

void Test_Complex::test_sum()
{
    int a = 10; int b = 10; int c = 5; int d = 5;
        QCOMPARE("15;15", sum(a, b, c, d));

}

void Test_Complex::test_minus()
{
    int a = 10; int b = 10; int c = 5; int d = 5;
        QCOMPARE("5;5", minus(a, b, c, d));

}

void Test_Complex::test_mult()
{
    int a = 10; int b = 10; int c = 5; int d = 5;
        QCOMPARE("75;100", mult(a, b, c, d));

}

void Test_Complex::test_div()
{
    int a = 10; int b = 10; int c = 5; int d = 5;
        QCOMPARE("2;0", div(a, b, c, d));

}

void Test_Complex::test_deg()
{
    int a = 1; int b = 2; int n = 2;
        QCOMPARE("-4.2;5.6", deg(a, b, n));

}

void Test_Complex::test_sqrt()
{
    int a = 1; int b = 2; int n = 2;
        QCOMPARE("5.95456;3.68012", sqrt(a, b, n));

}
